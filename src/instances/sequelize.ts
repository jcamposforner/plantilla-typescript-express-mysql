import * as Sequelize from 'sequelize'

const db = 'test'
const username = 'jesus'
const password = '1234'

export const sequelize = new Sequelize(db, username, password, {
  dialect: "mysql",
  port: 3306,
});

sequelize.authenticate()